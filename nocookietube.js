// from https://gist.github.com/deunlee/0b45cfacb7e8f788e5bbfa2911f54d3e
function getYouTubeVideoIdByUrl(url) {
    const reg = /^(https?:)?(\/\/)?((www\.|m\.)?youtube(-nocookie)?\.com\/((watch)?\?(feature=\w*&)?vi?=|embed\/|vi?\/|e\/)|youtu.be\/)([\w\-]{10,20})/i
    const match = url.match(reg);
    if (match) {
        return match[9];
    } else {
        return null;
    }
}

document.addEventListener('click', function(e) {
  var target = e.target

  while (target && target.tagName !== 'A') {
    target = target.parentNode;
    if (!target) { return; }
  }
  ytVideoId = getYouTubeVideoIdByUrl(target.href)
  if (ytVideoId) {
    console.log("Target object before: ")
    console.log(target)
    console.log("Video ID: " + ytVideoId)
    console.log(target.classList)
    if (target.classList.contains('ytp-suggestion-link')) {
        console.log("Suggestion link")
        target.href = ytVideoId // since the target.origin='www.youtube-nocookie.com/embed/' it suffices to set target.href=ytVideoId
    } else if (target.classList.contains('ytp-copylink-button')) {
        console.log("Copy Link Buttton")
        target.href = 'www.youtube-nocookie.com/embed/' + ytVideoId
    } else if (target.classList.contains('ytp-impression-link')) {
        console.log("Impression Link")
        target.href = 'www.youtube-nocookie.com/embed/' + ytVideoId
    } else if (target.classList.contains('ytp-youtube-button')) {
        console.log("YouTube Link")
        target.href = 'www.youtube-nocookie.com/embed/' + ytVideoId
    }
    //target.style = "transition-delay: 0.05s;"
    console.log("Target object after: ")
    console.log(target)
  }
}, false);


// Notes
// class where nocookie link works: 'ytp-suggestion-link'
// copy link button classes: 'ytp-button ytp-copylink-button ytp-show-copylink-title ytp-copylink-button-visible'
