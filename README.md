# NoCookieTube

A Firefox plugin to replace the youtube.com domain in youtube links with the youtube-nocookie.com domain

## Installation
1. Clone this repo
2. In Firefox, go to 'about:debugging'
3. Select 'This Firefox'
4. Click 'Load Temporary Add-on...'
5. Select any file in the NoCookieTube repo

## Usage
- Enter a Youtube link in the URL bar
- Click on a Youtube link on any website
If the video is embeddable using youtube-nocookie.com the video is opened there instead.

## Debugging
- Open the web developer tools (Ctrl + Shift + i) on the website you want to debug
- After modifying the extension reload it in 'about:debugging'

## Roadmap
the initial version only works for clicking on suggestion links in the embedded youtube player
- extend to all links
- extend to opening links from external links
- extend to opening links from url/search bar
- 
## License
MIT
